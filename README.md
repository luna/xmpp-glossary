# xmpp-glossary

Glossary of terms for XMPP.

## C

Conversations: A XMPP Client for Android. Built to compete with propietary alternatives. [Official page](https://conversations.im/).

## D

Dino: A XMPP Client written in GTK+ for Linux. [Official page](https://dino.im/#download).

## E

ejabberd: A XMPP Server written in Erlang. [Official page](https://ejabberd.im/).

## G

Gajim: A GTK+ client, written in Python. [Project on their own GitLab instance](https://dev.gajim.org/gajim/gajim).

Gajimbo: A fork of Gajim, maintained by [Ave](https://git.a3.pm/a). [Repository](https://git.a3.pm/a/gajimbo).

## H

HTTP File Upload: A XEP describing file uploads taking place a HTTP based API. Solves problems with Jingle.
[XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html).

## J

Jabber: XMPP's original name. [Original Slashdot post on Jabber](https://slashdot.org/story/99/01/04/1621211/open-real-time-messaging-system).

Jingle: XEP describing P2P sessions for any kind of media transfer. [XEP-0166: Jingle](https://xmpp.org/extensions/xep-0166.html).

## M

Multi-User Chat (MUC): As the name implies, MUCs are conversation channels
where more than two users can be in. [Unofficial listing of public MUCs](https://muclumbus.jabbercat.org/rooms/1).

Metronome: A XMPP Server written in Lua, forked off Prosody. [Official page](https://metronome.im/).

## O

OMEMO Multi-End Message and Object Encryption: An extension to XMPP that allows
encryption with the Signal Protocol. [info here](https://conversations.im/omemo/).

Over The Record (OTR) Messaging: An encryption protocol for Instant Messaging.
[Wikipedia page](https://en.wikipedia.org/wiki/Off-the-Record_Messaging). [XEP-0364: Current Off-the-Record Messaging Usage](https://xmpp.org/extensions/xep-0364.html).

## P

Prosody: A XMPP Server written in Lua. [Official page](https://prosody.im/).
ProtoXEP: [XEP Process](https://xmpp.org/about/standards-process.html).

## X

XEP: XMPP Protocol Extension. All XEPs can be found [here](https://github.com/xsf/xeps).

XMPP: [Extensible Messaging and Presence Protocol](https://en.wikipedia.org/wiki/XMPP).

XSF: [XMPP Standards Foundation](https://github.com/xsf).
